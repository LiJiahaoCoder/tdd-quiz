package com.twuc.webApp;

import java.util.*;

public class Storage {
    private Map<Ticket, Bag> bags = new HashMap<>();
    private final int capacity;
    private Map<String, Integer> typeCapacities = new HashMap<>();
//    private final int small;
//    private final int medium;
//    private final int big;
    private List<String> TYPE = Arrays.asList("small", "medium", "big");

    public Storage() {
        this(20, 0, 0);
    }

    public Storage(int small) {
        this(small, 0, 0);
    }

    public Storage(int small, int medium, int big) {
        this.typeCapacities.put("small", small);
        this.typeCapacities.put("medium", medium);
        this.typeCapacities.put("big", big);
        this.capacity = small + medium + big;
    }

    public Ticket save(Bag bag, String type) {
        if (TYPE.indexOf(bag.getType()) > TYPE.indexOf(type))
            throw new IllegalArgumentException(String.format("Can not save your bag: %s %s", bag.getType(), type));

        Ticket ticket = new Ticket();
        if (typeCapacities.get(type) <= this.bags.size()) {
            throw new IllegalArgumentException("Insufficient capacity");
        }
        this.bags.put(ticket, bag);
        this.typeCapacities.put(type, this.typeCapacities.get(type) - 1);
        return ticket;
    }

    public Ticket save(Bag bag) {
        Ticket ticket = new Ticket();
        if (this.capacity == this.bags.size()) {
            throw new IllegalArgumentException("Insufficient capacity");
        }
        this.bags.put(ticket, bag);
        return ticket;
    }

    public Bag retrieve(Ticket ticket) {
        boolean containsKey = this.bags.containsKey(ticket);
        if (!containsKey) {
            throw new IllegalArgumentException("Invalid Ticket");
        }

        Bag removeBag = this.bags.remove(ticket);
        return removeBag;
    }
}
