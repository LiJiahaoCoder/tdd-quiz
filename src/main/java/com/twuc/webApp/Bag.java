package com.twuc.webApp;

public class Bag {
    private final String type;

    public Bag() {
        this("small");
    }

    public Bag(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
