package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StorageTest {
    // 抽流程不抽数据
    // story 1
    @Test
    void should_get_the_ticket_when_saving_a_bag() {
        Storage storage = new Storage();
        Ticket ticket = storage.save(new Bag());
        assertNotNull(ticket);
    }

    @Test
    void should_get_the_ticket_when_saving_nothing() {
        Storage storage = new Storage();
        Ticket ticket = storage.save(null);
        assertNotNull(ticket);
    }

    @Test
    void should_get_the_ticket_when_saving_two_bags() {
        Storage storage = new Storage();
        Bag savedFirstBag = new Bag();
        Ticket firstTicket = storage.save(savedFirstBag);

        storage.save(new Bag());

        Bag retrievedFirstBag = storage.retrieve(firstTicket);
        assertSame(savedFirstBag, retrievedFirstBag);
    }

    @Test
    void should_get_the_saved_bag_when_given_a_valid_ticket() {
        Storage storage = new Storage();
        Bag savedBag = new Bag();
        Ticket ticket = storage.save(savedBag);

        Bag retrievedBag = storage.retrieve(ticket);
        assertSame(savedBag, retrievedBag);
    }

    // TODO:
    // 2. 取过之后再使用之前的Ticket取


    @Test
    void should_get_error_message_when_given_a_null_ticket() {
        Storage storage = new Storage(2);
        storage.save(new Bag());

        assertThrows(
                IllegalArgumentException.class,
                () -> storage.retrieve(null),
                "Invalid Ticket"
        );
    }

    @Test
    void should_get_error_message_when_given_an_invalid_ticket() {
        Storage storage = new Storage();
        // save a bag
        storage.save(new Bag());

        Ticket invalidTicket = new Ticket();

        assertThrows(
                IllegalArgumentException.class,
                () -> storage.retrieve(invalidTicket),
                "Invalid Ticket"
        );
    }

    @Test
    void should_get_nothing_when_given_valid_ticket_but_saving_nothing() {
        Storage storage = new Storage();
        Ticket ticket = storage.save(null);

        Bag bag = storage.retrieve(ticket);

        assertNull(bag);
    }

    // story 2

    @Test
    void should_get_a_ticket_when_saving_a_bag_in_an_empty_storage() {
        Storage storage = new Storage(2);
        Ticket ticket = storage.save(new Bag());

        assertNotNull(ticket);
    }

    @Test
    void should_get_a_message_if_save_a_bag_into_a_full_storage() {
        Storage storage = new Storage(0);

        assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(new Bag()),
                "Insufficient capacity"
        );
    }

    @Test
    void should_get_a_message_when_saving_2_bags_one_by_one() {
        Storage storage = new Storage(1);
        storage.save(new Bag());

        assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(new Bag()),
                "Insufficient capacity"
        );
    }

    @Test
    void should_get_a_ticket_when_a_full_storage_is_available() {
        Storage storage = new Storage(1);
        Ticket firstTicket = storage.save(new Bag());
        storage.retrieve(firstTicket);

        Ticket secondTicket = storage.save(new Bag());
        assertNotNull(secondTicket);
    }
    
    // story 3

    @Test
    void should_get_a_valid_ticket_when_save_a_small_bag_to_a_big_slot() {
        Storage storage = new Storage(0, 0, 1);
        Ticket ticket = storage.save(new Bag("small"), "big");

        assertNotNull(ticket);
    }

    @Test
    void should_get_error_message_when_save_a_big_bag_into_a_small_slot() {
        Storage storage = new Storage(1, 0, 0);

        assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(new Bag("big"), "small"),
                "Can not save your bag: big small"
        );
    }

    @Test
    void should_get_message_when_save_a_big_bag_to_a_full_big_slot() {
        Storage storage = new Storage(1, 1, 0);

        assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(new Bag("big"), "big"),
                "Insufficient capacity"
        );
    }

    @Test
    void should_get_a_valid_ticket_when_save_a_medium_bag_into_a_medium_slot() {
        Storage storage = new Storage(0, 1, 0);
        Ticket ticket = storage.save(new Bag("medium"), "medium");

        assertNotNull(ticket);
    }
}