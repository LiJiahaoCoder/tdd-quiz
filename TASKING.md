# TASKING

### Story 1

* Given a storage and a bag. When saving the  bag, then I should be able to get the ticket.
* Given a storage. When saving nothing, then I should be able to get the ticket.
* Given a valid ticket(the ticket is created via saving a bag, and the ticket has not been used), when retrieving the bag using the ticket, then I should be able to get the saved bag.
* Given an invalid ticket. When retreving the bag, then I should be able to get an error message('Invalid Ticket').
* Given a valid ticket (saving nothing), when retrieving the bag, then I should get nothing.

### Story 2

* 